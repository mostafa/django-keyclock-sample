from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
import requests
import json
class CniliKeycloakMiddleware(MiddlewareMixin):
    def process_request(self, request):
        code = request.GET.get('code')
        if code:
            if 'code_challenge' in request.session:
                data = {
                    "code" : code,
                    "grant_type" : "authorization_code",
                    "client_id" : settings.KEYCLOAK_CONFIG['CLIENT_ID'],
                    "client_secret" : settings.KEYCLOAK_CONFIG['CLIENT_SECRET_KEY'],
                    "redirect_uri" : request.build_absolute_uri(settings.KEYCLOAK_VALIDATE_URL),
                    "code_challenge_method" : "S256",
                    "code_verifier" : request.session['code_challenge']
                }
                url = settings.KEYLOACK_LOGIN_URL.format(settings.KEYCLOAK_CONFIG['SERVER_URL'],
                                                        settings.KEYCLOAK_CONFIG['REALM'],
                                                        'token',
                                                        settings.KEYCLOAK_CONFIG['CLIENT_ID'],
                                                        (request.build_absolute_uri(settings.KEYCLOAK_VALIDATE_URL)),
                                                        '',
                                                        code)
                c = requests.post(url, data=data)
                data = json.loads(c.content)
                request.META['HTTP_AUTHORIZATION'] = "Bearer %s" % data['access_token']
                request.session['HTTP_AUTHORIZATION'] =  data['access_token']
