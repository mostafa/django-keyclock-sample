from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
import pkce
import redis
import requests
import json



def logout_view(request):
    logout(request)
    return HttpResponse("logedout")


# Create your views here.
def login_view(request):
    code = pkce.generate_code_verifier(length=128)
    state = pkce.get_code_challenge(code)
    url = settings.KEYLOACK_LOGIN_URL.format(settings.KEYCLOAK_CONFIG['SERVER_URL'],
                                             settings.KEYCLOAK_CONFIG['REALM'],
                                             'auth',
                                             settings.KEYCLOAK_CONFIG['CLIENT_ID'],
                                            (request.build_absolute_uri(settings.KEYCLOAK_VALIDATE_URL)),
                                             state,
                                             code
                                                                      )
    request.session['code_challenge'] = code
    return redirect(url)



def validate(request):
    token = {
        'token' : request.META['HTTP_AUTHORIZATION']
    }
    return HttpResponse(json.dumps(token), content_type="application/json")

@login_required
def index(request):
    print(request.user)
    return HttpResponse("Hello world!")
