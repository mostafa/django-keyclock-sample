"""sso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from cnili import views
from cnili import user_views
from django.shortcuts import render
from cnili.urls import router


urlpatterns = [
    re_path(r'api/v1/', include(router.urls)),
    path('drf/login/', user_views.LoginView.as_view()),
    path('keycloak/login/', views.login_view, name='login_view'),
    path('logout', views.logout_view, name='logout_view'),
    path('validate', views.validate, name='validate_view'),
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
]
